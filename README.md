# go-monkey

[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

[//]: # (TODO description )
> Implementing the Monkey language from [Writing An Interpreter In Go](https://interpreterbook.com/).

## Install

    git clone https://you@bitbucket.org/mreil/go-monkey.git 
    

## Usage

Follow my progress step by step while I complete the book.


## Contributing

Feel free to dive in! [Open an issue][issues] or raise a [Pull Request][pr].


## License

[//]: # (TODO Choose an appropriate license type and make sure the link points a valid license file. )
[MIT][license]


[issues]: ../../issues/
[pr]: ../../pull-requests/
[license]: LICENSE.txt
